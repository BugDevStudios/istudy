


// Initialize your app
var myApp = new Framework7({material:true});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});
var live_url = 'http://istudy.com.pk/';


setTimeout(function(){
    mainView.router.loadPage("login.html");
}, 500);

// Callbacks to run specific code for specific pages, for example for About page:
myApp.onPageInit('login', function (page) {
    //myApp.hideIndicator();
    if (getItem("app_user_id") != '') {
        //myApp.showIndicator();
        if (getItem("app_user_level") == 'student' && getItem("app_user_id") > '0' && localStorage.getItem("app_user_status") == '1') {
            $$('#aboutmepage').hide();
            $$("#home").attr("href", "student_profile.html");
            call_after_n_seconds('student_profile.html',0.5);
        } else if (getItem("app_user_level") == 'teacher' && getItem("app_user_id") > '0' && getItem("app_user_status") == '1') {
            $$('#aboutmepage').show();
            $$("#home").attr("href", "teacher_profile.html");
            call_after_n_seconds('teacher_profile.html',0.5);
        } else {
            $$(".parent-div").show();
            //myApp.hideIndicator();
            setItem("app_user_id", '');
            setItem("app_username", '');
            setItem("app_phone", '');
            setItem("app_user_level", '');
            setItem("app_user_status", '');
            mainView.router.loadPage('login.html');
        }
    } else {
        $$(".parent-div").show();
    }
});

myApp.onPageInit('cart', function (page) {
    loadCart();
});

myApp.onPageInit('about-me', function (page) {
    $$('.teacher_username').html(getItem('app_username'));
    $$('.teacher_phone').html(getItem('app_phone'));
    $$('.teacher_email').html(getItem('app_email'));
    getExperience();
});

myApp.onPageInit('student_profile', function (page) {
    myApp.hideIndicator();
});

myApp.onPageInit('student_courses', function (page) {
    getStudentCourses();
});

myApp.onPageInit('student_statements', function (page) {
    getStudentStatements();
});

myApp.onPageInit('student_profile_settings', function (page) {
    $$('.form-to-data').on('click', function(){
        updateStudentData();
    }); 

    var myCalendar = myApp.calendar({
        input: '#birthday',
        dateFormat : 'yyyy-mm-dd'
    });

    getStudentProfile();
});

myApp.onPageInit('view_course', function (page) {
    stopEverything();
    myApp.hideIndicator();
    viewCourseDetails();
    // setTimeout(function(){
    //     console.log('called');
    //     document.getElementsByClassName('drive-viewer-toolstrip')[0].style.visibility='hidden';
    // }, 10000);
});

myApp.onPageInit('course_content', function (page) {
    myApp.hideIndicator();
    $$('.text-heading').html(getItem('doc_title'));

    if (getItem('doc_type')=='audio'){
        var html = '<audio controls style="display: inline !important; width: 95%" autostart="0" class="myaudio"><source src="'+getItem('doc_url')+'" type="audio/mpeg">Your browser does not support the audio element.</audio>';
        $$('.doc').html(html);
    } else if (getItem('doc_type')=='video'){
        var html = '<iframe frameborder="0" allowfullscreen="1" title="'+getItem('doc_title')+'" width="95%" height="auto" src="'+getItem('doc_url')+'"></iframe>';
        $$('.doc').html(html);
    } else {
        var ref = cordova.InAppBrowser.open('https://drive.google.com/open?id=0B19Eui0snaeLT2pXSWE4ZGhFdXc', '_blank', 'location=yes');
        ref.addEventListener('loadstop', function(event) {
         myApp.alert('called', "Oh Snap! :(", null);
     });
        /*
        var html = '<iframe src="https://docs.google.com/viewer?embedded=true&url='+getItem('doc_url')+'" frameborder="no" style="width:95%;height:500px"></iframe>';
        $$('.doc').html(html);*/
    }

});

myApp.onPageInit('teacher_profile', function (page) {
    myApp.hideIndicator();
});

myApp.onPageInit('teacher_courses', function (page) {
    getTeacherCourses();
});

myApp.onPageInit('teacher_statements', function (page) {
    getTeacherStatements();
});

myApp.onPageInit('teacher_profile_settings', function (page) {
    $$('.form-to-data').on('click', function(){
        updateTeacherData();
    }); 

    var myCalendar = myApp.calendar({
        input: '#birthday',
        dateFormat : 'yyyy-mm-dd'
    });

    //getSchools();
    getTeacherProfile();
});

myApp.onPageInit('coursesDetail', function (page) {
    getCoursesDetail();
});

myApp.onPageInit('signup', function (page) {
    getCountries();

    var myCalendar = myApp.calendar({
        input: '#birthday',
        dateFormat : 'yyyy-mm-dd'
    });

});

/* Custom Functions Start */

function setItem(key,value){
    localStorage.setItem(key,value);
}

function getItem(key){
    return localStorage.getItem(key);
}

function call_after_n_seconds(url,seconds){
    var t = seconds * 1000;
    setTimeout(function(){
        mainView.router.loadPage(url);
    }, t);
}



// function logout() {
//     setItem("app_user_id", '');
//     setItem("app_username", '');
//     setItem("app_phone", '');
//     setItem("app_user_level", '');
//     setItem("app_user_status", '');
//     setItem("country", '');
//     setItem("state", '');
//     setItem("city", '');
//     mainView.router.loadPage('login.html');
// }

/* Custom Functions End */

/* AJAX Request Function */

function serverRequest(requestDATA, successCallback, errorCallback, hideIndicator, showAlert){
    var METHOD = "POST";
    var serverURL = 'http://istudy.com.pk/api/server.php'; //Path to Server.php
    //var serverURL = 'http://localhost/istudy_server/server.php'; //Path to Server.php
    var DATA_TYPE = 'json';
    var TIMEOUT = 20000;
    console.log(requestDATA);
    $$.ajax({
        url: serverURL,
        data: requestDATA,
        dataType: DATA_TYPE,
        type: METHOD,
        timeout: TIMEOUT,
        success: function(data){
            successCallback(data, hideIndicator, showAlert);
        },
        error: function(a, b, c){
            errorCallback(a, b, c, hideIndicator, showAlert);
        }
    }); 
}

/* Function to handle request error */

function responseError(xhr, textStatus, errorThrown, hideIndicator, showAlert){
    console.log(xhr);
    console.log(textStatus);
    console.log(errorThrown);
    var error_message = "";
    switch(textStatus){
        case 'error':
        error_message = "Please check your internet connection and try again.";
        break;
        case 'parsererror':
        error_message = "Internal error occurred. Report application administrator about this error.";
        break;
        case 'timeout':
        error_message = "Slow internet may be. Pull down to refresh page.";
        break;
        case 'abort':
        error_message = "The request was aborted.";
        break;
        default:
        error_message = "Cannot reach server at this time. You can report this error.";
        break;
    }
    if(hideIndicator){
        myApp.hideIndicator();
    }

    if(showAlert){
        myApp.alert(error_message, "Oh Snap! :(", null);
    }
}

/* Request With Server Fail or Error or Others */

function requestFailure(status, message, showAlert){
    if(showAlert){
        if(status == 'error'){
            myApp.alert(message, 'Error', null);
        } else if(status == ''){
            myApp.alert(message, 'Request Failure!', null);
        } else {
            if(showAlert){
                myApp.alert("Application was not ready to serve this request at this time.", 'Unknown Response', null);
            }
        }
    }
}

/* Requests Start */

// Login
function login(){

    if ($$('#Email').val() == '' || $$('#Password').val() == ''){
        myApp.alert('Please fill all fields.', "Alert", null);
    } else {
        var requestDATA = {
            'user_type': $$('input[name=UserType]:checked').val(),
            'email': $$('#Email').val(),
            'password': $$('#Password').val(),
            'request': 'user_login'
        }
        myApp.showIndicator();
        serverRequest(requestDATA, loginSuccess, responseError, true, true);
    };
}

function loginSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){

        setItem("app_user_id", data.user_data['app_user_id']);
        setItem("app_username", data.user_data['app_username']);
        setItem("app_email", data.user_data['app_email']);
        setItem("app_phone", data.user_data['app_phone']);
        setItem("app_user_level", data.user_data['app_user_level']);
        setItem("app_user_status", data.user_data['app_user_status']);
        setItem("app_image", data.user_data['img_url']);

        if (data.redirect == 'student') {
            $$('#aboutmepage').hide();
            mainView.router.loadPage("student_profile.html");
        } else if (data.redirect == 'teacher') {
            $$('#aboutmepage').show();
            mainView.router.loadPage("teacher_profile.html");
        }


    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

//Logout

function logout(){
    
    var requestDATA = {
            'app_user_id': getItem('app_user_id'),
            'app_user_level': getItem('app_user_level'),
            'request': 'logout'
        }
    myApp.showIndicator();
    serverRequest(requestDATA, logoutSuccess, responseError, true, true);
}

function logoutSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        setItem("app_user_id", '');
        setItem("app_username", '');
        setItem("app_phone", '');
        setItem("app_user_status", '');
        setItem("country", '');
        setItem("state", '');
        setItem("city", '');
        mainView.router.loadPage('login.html');

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}


function getStudentCourses(){
    var requestDATA = {
        'app_user_id': getItem('app_user_id'),
        'request': 'student_courses',
        'page': '1',
        'limit': '999999999'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getStudentCoursesSuccess, responseError, true, true);
}

function getStudentCoursesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$("#subjects").html('');
        for (var i = 0; i < data.courses_data.length; i++) {
            img = 'img/subjectPicture.png';
            
            //text = '<a href="view_course.html?course_id=' + data.courses_data[i].course_id + '" class="no_link"><div class="row subjectDetail"><div class="col-xs-4"><img src="' + img + '" class="subjectPicture"></div><div class="col-xs-8 col-info"><h5><img src="img/My Courses/Subject icon.png" class="subjectIcon"> ' + data.courses_data[i].course_title + '</h5><p>By ' + data.courses_data[i].teacher_name + '</p><p>Rs, ' + data.courses_data[i].price + '</p><p><img src="img/My Courses/Pupil Icon.png" class="pupilIcon"> ' + data.courses_data[i].class_name + '</p><div class="code"><p class="personName">' + data.courses_data[i].course_code + '</p></div></div></div></a>';
            text = '<li>';
            text += '<a href="#" onclick="viewCourse('+data.courses_data[i].course_id+');" class="item-link item-content">';
            text += '<div class="item-media"><img src="'+img+'" class="picture-icon" width="44" height="44"></div>';
            text += '<div class="item-inner">';
            text +='<div class="item-title-row">';
            text += '<div class="item-title">' + data.courses_data[i].course_title + '</div>';
            text += '</div>';
            text += '<div class="item-subtitle">Teacher: ' + data.courses_data[i].teacher_name + '</div>';
            text += '<div class="item-subtitle">Rs. ' + data.courses_data[i].price + '</div>';
            text += '<div class="item-subtitle">Code: ' + data.courses_data[i].course_code + '</div>';
            text += '<div class="item-subtitle">' + data.courses_data[i].class_name + '</div>';
            text += '</div></a></li>';
            $$("#subjects").append(text);
        }

    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function viewCourse(id){
    setItem("currentCourseId",id);
    mainView.router.loadPage('view_course.html');
}

function viewCourseDetails(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'course_id': localStorage.getItem('currentCourseId'),
        'request': 'view_course',
    }
    myApp.showIndicator();
    serverRequest(requestDATA, viewCourseDetailsSuccess, responseError, true, true);

}

function viewCourseDetailsSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        if (data.course.length > 0) {
            text = '<ul class="list-group" style="padding:20px; background-color: transparent; border: 0px;">';
            text += '<li class="list-group-item"><b>Course Title: </b>' + data.course[0].course_title + '</li>';
            text += '<li class="list-group-item"><b>Course Code: </b>' + data.course[0].course_code + '</li>';
            text += '<li class="list-group-item"><b>Course Price: </b>' + data.course[0].course_price + '</li>';
            text += '<li class="list-group-item"><b>Course Expiry: </b>' + data.course[0].course_expiry + '</li>';
            text += '</ul>';

            $$("#doc_detail").append(text);
        }
        for (var i = 0; i < data.docs.length; i++) {
            if (data.docs[i].doc_type == 'doc') {
                text = '<div class="row text-center">';
                //text += '<a href="' + live_url + 'assets/3f05cc5ff16a157b57338f1262aeb0ae/' + data.docs[i].doc_file_content + '" class="embedURL">' + data.docs[i].doc_title + '</a>';
                text += '<iframe src="https://drive.google.com/viewerng/viewer?url=https://library.osu.edu/assets/Documents/SEL/QuickConvertWordPDF.pdf?pid=explorer&efh=false&a=v&chrome=false&embedded=true" width="400px" height="300px" />';
                text += '</div><br />';
                text = '<a href="#" onClick="loadPDF(\'https://drive.google.com/file/d/0B_nipvep1WpPd2JXeDdJcUlNYXM/view\')"><div class="chip" style="width:100%"><div class="chip-media bg-teal">P</div><div class="chip-label">'+data.docs[i].course_title+'</div></div></a>'
            } else if (data.docs[i].doc_type == 'audio') {
                text = '<div class="row text-center">';
                text += '<audio controls style="display: inline !important; width: 100%;">';
                text += '<source src="' + data.docs[i].doc_file_content + '" type="audio/mpeg">';
                text += 'Your browser does not support the audio element.';
                text += '</audio></div><br />';
                text = '<a href="#" onClick="loadDoc("audio",'+data.docs[i].doc_file_content+','+data.docs[i].course_title+')"><div class="chip" style="width:100%"><div class="chip-media bg-teal">A</div><div class="chip-label">'+data.docs[i].course_title+'</div></div></a>'
            } else if (data.docs[i].doc_type == 'video') {
                text = '<div class="row text-center">';
                text += '<video  width="95%" height="auto" controls>';
                text += '<source src="' + data.docs[i].doc_file_content + '" type="video/mp4">';
                text += 'Your browser does not support the video element.';
                text += '</video></div><br />';
                text = '<a href="#" onClick="loadDoc("video",'+data.docs[i].doc_file_content+','+data.docs[i].course_title+')"><div class="chip" style="width:100%"><div class="chip-media bg-teal">V</div><div class="chip-label">'+data.docs[i].course_title+'</div></div></a>'
            }
            $$("#documents").append(text);
        }

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}
function getStudentStatements(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'student_statements',
        'page': '1',
        'limit': '999999'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getStudentStatementsSuccess, responseError, true, true);
}

function getStudentStatementsSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('#statements').html();

        if (data.statements.length > 0){
            var total_price = 0;
            for (var i = 0; i < data.statements.length; i++) {
                title = data.statements[i].course_title;
                course_status = data.statements[i].status;
                if (course_status == '1') {
                    status = 'Paid';
                    course_class = 'paidStatus';
                    icon = 'paid.png';
                    colorclass = 'green';
                } else {
                    status = 'UnPaid';
                    course_class = 'notPaidStatus';
                    icon = 'unpaid.png';
                    colorclass = 'red';
                }
                order_num = data.statements[i].order_num;
                date_buy = data.statements[i].date_buy;
                price = data.statements[i].price;
                total_price = Number(total_price) + Number(price);
                var text = '<div class="list-block media-list list-of-title">';
                if (i==0){
                    text += '<ul id="top-line">';
                } else {
                    text += '<ul>';
                }

                text += '<li>';
                text += '<a href="#" class="item-link item-content">';
                text += '<div class="item-media"><img src="img/'+icon+'" class="picture-icon" width="44"></div>';
                text += '<div class="item-inner">';
                text += '<div class="item-title-row">';
                text += '<div class="item-title">'+title+'</div>';
                text += '<div class="item-after">Rs. '+price+'</div>';
                text += '</div>';
                text += '<div class="item-subtitle">'+status+'</div>';
                text += '<div class="row">';
                text += '<div class="col-50">';
                text += '<div class="item-text">Order # '+order_num+'</div>';
                text += '</div>';
                text += '<div class="col-50">';
                text += '<div class="item-text">'+date_buy+'</div>';
                text += '</div>';
                text += '</div>';
                text += '</div>';
                text += '</a>';
                text += '</li>';
                text += '</ul>';
                text += '</div>';
                $$('#statements').append(text);
            }
            var text = '<div class="list-block media-list list-of-title">';
            text += '<ul id="top-line">';
            text += '<li>';
            text += '<a href="#" class="item-link item-content">';
            text += '<div class="item-media"></div>';
            text += '<div class="item-inner">';
            text += '<div class="item-title-row">';
            text += '<div class="item-title"></div>';
            text += '<div class="item-after">Total: Rs. '+total_price+'</div>';
            text += '</div>';
            text += '<div class="item-subtitle"></div>';
            text += '<div class="row">';
            text += '<div class="col-50">';
            text += '<div class="item-text"></div>';
            text += '</div>';
            text += '<div class="col-50">';
            text += '<div class="item-text"></div>';
            text += '</div>';
            text += '</div>';
            text += '</div>';
            text += '</a>';
            text += '</li>';
            text += '</ul>';
            text += '</div>';
            $$('#statements').append(text);
        }


    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

// function getSchools(){
//  var requestDATA = {
//      'request': 'get_schools'
//  }
//  myApp.showIndicator();
//  serverRequest(requestDATA, getSchoolsSuccess, responseError, true, true);
// }

// function getClasses(){
//  var requestDATA = {
//      'request': 'get_classes'
//  }
//  myApp.showIndicator();
//  serverRequest(requestDATA, getClassesSuccess, responseError, true, true);
// }

// function getCountries(){
//  var requestDATA = {
//      'request': 'get_countries'
//  }
//  myApp.showIndicator();
//  serverRequest(requestDATA, getCountriesSuccess, responseError, true, true);
// }

// function getStates(){
//  var requestDATA = {
//      'request': 'get_states'
//  }
//  myApp.showIndicator();
//  serverRequest(requestDATA, getStatesSuccess, responseError, true, false);
// }

// function getCities(){
//  var requestDATA = {
//      'request': 'get_cities'
//  }
//  myApp.showIndicator();
//  serverRequest(requestDATA, getCitiesSuccess, responseError, true, false);
// }

// function getSchoolsSuccess(data, hideIndicator, showAlert){
//  if(hideIndicator){
//      myApp.hideIndicator();
//  }
//  console.log(data);

//  if(data.status == 'success'){
//  }else if (data.status == 'error'){
//      requestFailure(data.status, data.message, showAlert);
//  } else if (data.status == '') {
//      requestFailure(data.status, data.message, showAlert);
//  }
// }

// function getClassesSuccess(data, hideIndicator, showAlert){
//  if(hideIndicator){
//      myApp.hideIndicator();
//  }
//  console.log(data);

//  if(data.status == 'success'){
//  }else if (data.status == 'error'){
//      requestFailure(data.status, data.message, showAlert);
//  } else if (data.status == '') {
//      requestFailure(data.status, data.message, showAlert);
//  }
// }


// function getCountriesSuccess(data, hideIndicator, showAlert){
//  if(hideIndicator){
//      myApp.hideIndicator();
//  }
//  console.log(data);

//  if(data.status == 'success'){
//  }else if (data.status == 'error'){
//      requestFailure(data.status, data.message, showAlert);
//  } else if (data.status == '') {
//      requestFailure(data.status, data.message, showAlert);
//  }
// }


// function getStatesSuccess(data, hideIndicator, showAlert){
//  if(hideIndicator){
//      myApp.hideIndicator();
//  }
//  console.log(data);

//  if(data.status == 'success'){
//  }else if (data.status == 'error'){
//      requestFailure(data.status, data.message, showAlert);
//  } else if (data.status == '') {
//      requestFailure(data.status, data.message, showAlert);
//  }
// }

// function getCitiesSuccess(data, hideIndicator, showAlert){
//  if(hideIndicator){
//      myApp.hideIndicator();
//  }
//  console.log(data);

//  if(data.status == 'success'){
//  }else if (data.status == 'error'){
//      requestFailure(data.status, data.message, showAlert);
//  } else if (data.status == '') {
//      requestFailure(data.status, data.message, showAlert);
//  }
// }

function getStudentProfile(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'student_profile'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getStudentProfileSuccess, responseError, true, true);
}

function getStudentProfileSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('#name').val(data.form_data['name']);
        $$('#email').val(data.form_data['email']);
        $$('#phone').val(data.form_data['phone']);
        $$('#school').val(data.form_data['school']);
        $$('#gender').val(data.form_data['gender']);
        $$('#class').val(data.form_data['class']);
        $$('#birthday').val(data.form_data['birthday']);
        $$('#address').val(data.form_data['address']);
        $$('#zip').val(data.form_data['zip']);


        setItem('school',data.form_data['school']);
        setItem('class',data.form_data['class']);
        setItem('country',data.form_data['country']);
        setItem('state',data.form_data['state']);
        setItem('city',data.form_data['city']);

        getSchools();
        getClasses();
        getCountries();

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getStudentProfileExtraData(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'student_profile_extra_data'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getStudentProfileSuccess, responseError, true, true);
}

function getStudentProfileExtraDataSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function updateStudentData(){
    if (($$('#name').val() == '') || ($$('#email').val() == '') || ($$('#phone').val() == '') || ($$('#gender').val() == '') || ($$('#birthday').val() == '') || ($$('#address').val() == '') || ($$('#zip').val() == '') || ($$('#country_id').val() == '') || ($$('#state_id').val() == '')){
        myApp.alert('Please fill all fields.','Error',null);
    } else {
        var requestDATA = {
            'app_user_id': localStorage.getItem('app_user_id'), //Store name fields value
            'request': 'update_student_data',
            'name': $$('#name').val(),
            'email': $$('#email').val(),
            'phone': $$('#phone').val(),
            'gender': $$('#gender').val(),
            'school': $$('#school_id').val(),
            'class': $$('#class_id').val(),
            'country': $$('#country_id').val(),
            'state': $$('#state_id').val(),
            'city': $$('#city_id').val(),
            'birthday': $$('#birthday').val(),
            'address': $$('#address').val(),
            'zip': $$('#zip').val()
        }
        myApp.showIndicator();
        serverRequest(requestDATA, updateStudentDataSuccess, responseError, true, true);
    }
}

function updateStudentDataSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        myApp.alert(data.message,'Success', mainView.router.loadPage('student_profile_settings.html'));
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getTeacherCourses(){
    var requestDATA = {
        'app_user_id': getItem('app_user_id'),
        'request': 'teacher_courses',
        'page': '1',
        'limit': '999999999'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getTeacherCoursesSuccess, responseError, true, true);
}

function getTeacherCoursesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$("#subjects").html('');
        for (var i = 0; i < data.courses_data.length; i++) {
            if (data.courses_data[i].course_image == '') {
                img = 'img/subjectPicture.png';
            } else {
                img = 'http://193.42.156.121/istudy/assets/images/course/' + data.courses_data[i].course_image;
            }
            //text = '<a href="view_course.html?course_id=' + data.courses_data[i].course_id + '" class="no_link"><div class="row subjectDetail"><div class="col-xs-4"><img src="' + img + '" class="subjectPicture"></div><div class="col-xs-8 col-info"><h5><img src="img/My Courses/Subject icon.png" class="subjectIcon"> ' + data.courses_data[i].course_title + '</h5><p>By ' + data.courses_data[i].teacher_name + '</p><p>Rs, ' + data.courses_data[i].price + '</p><p><img src="img/My Courses/Pupil Icon.png" class="pupilIcon"> ' + data.courses_data[i].class_name + '</p><div class="code"><p class="personName">' + data.courses_data[i].course_code + '</p></div></div></div></a>';
            text = '<li>';
            text += '<a href="#" class="item-link item-content">';
            text += '<div class="item-media"><img src="'+img+'" class="picture-icon" width="44"></div>';
            text += '<div class="item-inner">';
            text +='<div class="item-title-row">';
            text += '<div class="item-title">' + data.courses_data[i].course_title + '</div>';
            text += '</div>';
            text += '<div class="item-subtitle">' + data.courses_data[i].teacher_name + '</div>';
            text += '<div class="item-subtitle">' + data.courses_data[i].price + '</div>';
            text += '<div class="item-subtitle">' + data.courses_data[i].class_name + '</div>';
            text += '</div></a></li>';
            $$("#subjects").append(text);
        }
    }else if (data.status == 'error'){
        var text = '<p>No Courses Found.</p>';
        $$("#subjects").append(text);
        requestFailure(data.status, data.message);
    } else if (data.status == '') {
        $$("#subjects").html('');
        requestFailure(data.status, data.message, showAlert);
    }
}

function getTeacherStatements(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'teacher_statements',
        'page': '1',
        'limit': '999999'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getTeacherStatementsSuccess, responseError, true, true);
}

function getTeacherStatementsSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('#statements').html();

        if (data.statements.length > 0){
            var total_price = 0;
            for (var i = 0; i < data.statements.length; i++) {
                title = data.statements[i].course_title;
                course_status = data.statements[i].status;
                if (course_status == '1') {
                    status = 'Paid';
                    course_class = 'paidStatus';
                    icon = 'Paid Status Icon.png';
                } else {
                    status = 'UnPaid';
                    course_class = 'notPaidStatus';
                    icon = 'Not Paid Status Icon.png';
                }
                order_num = data.statements[i].order_num;
                date_buy = data.statements[i].date_buy;
                price = data.statements[i].price;
                total_price = Number(total_price) + Number(price);
                var text = '<div class="list-block media-list list-of-title">';
                if (i==0){
                    text += '<ul id="top-line">';
                } else {
                    text += '<ul>';
                }

                text += '<li>';
                text += '<a href="#" class="item-link item-content">';
                text += '<div class="item-media"><img src="img/f7-icon.png" class="picture-icon" width="44"></div>';
                text += '<div class="item-inner">';
                text += '<div class="item-title-row">';
                text += '<div class="item-title">'+title+'</div>';
                text += '<div class="item-after">Rs. '+price+'</div>';
                text += '</div>';
                text += '<div class="item-subtitle">'+status+'</div>';
                text += '<div class="row">';
                text += '<div class="col-50">';
                text += '<div class="item-text">Order # '+order_num+'</div>';
                text += '</div>';
                text += '<div class="col-50">';
                text += '<div class="item-text">'+date_buy+'</div>';
                text += '</div>';
                text += '</div>';
                text += '</div>';
                text += '</a>';
                text += '</li>';
                text += '</ul>';
                text += '</div>';
                $$('#statements').append(text);
            }
        }

        
    }else if (data.status == 'error'){
        var text = '<div class="content-block"><p>'+data.message+'</p></div>';
        $$('#statements').append(text);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getTeacherProfile(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'teacher_profile'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getTeacherProfileSuccess, responseError, true, true);
}

function getTeacherProfileSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('#name').val(data.form_data['name']);
        $$('#designation').val(data.form_data['designation']);
        $$('#teaching_since').val(data.form_data['teaching_since']);
        $$('#email').val(data.form_data['email']);
        $$('#phone').val(data.form_data['phone']);
        $$('#school').val(data.form_data['school']);
        $$('#gender').val(data.form_data['gender']);
        $$('#class').val(data.form_data['class']);
        $$('#birthday').val(data.form_data['birthday']);
        $$('#address').val(data.form_data['address']);
        $$('#zip').val(data.form_data['zip']);

        setItem('school',data.form_data['school']);
        setItem('class',data.form_data['class']);
        setItem('country',data.form_data['country']);
        setItem('state',data.form_data['state']);
        setItem('city',data.form_data['city']);

        getSchools();
        getClasses();
        getCountries();

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getTeacherProfileExtraData(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'student_profile_extra_data'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getTeacherProfileExtraDataSuccess, responseError, true, true);
}

function getTeacherProfileExtraDataSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){

    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function updateTeacherData(){
    if (($$('#name').val() == '') || ($$('#email').val() == '') || ($$('#phone').val() == '') || ($$('#gender').val() == '') || ($$('#teaching_since').val() == '') || ($$('#designation').val() == '') || ($$('#birthday').val() == '') || ($$('#address').val() == '') || ($$('#zip').val() == '')){
        myApp.alert('Please fill all fields.','Error',null);
    } else {
        var requestDATA = {
            'app_user_id': localStorage.getItem('app_user_id'), //Store name fields value
            'request': 'update_teacher_data',
            'name': $$('#name').val(),
            'email': $$('#email').val(),
            'phone': $$('#phone').val(),
            'gender': $$('#gender').val(),
            'teaching_since' : $$('#teaching_since').val(),
            'designation' : $$('#designation').val(),
            'school': $$('#school_id').val(),
            'class': $$('#class_id').val(),
            'country': $$('#country_id').val(),
            'state': $$('#state_id').val(),
            'city': $$('#city_id').val(),
            'birthday': $$('#birthday').val(),
            'address': $$('#address').val(),
            'zip': $$('#zip').val()
        }
        myApp.showIndicator();
        serverRequest(requestDATA, updateTeacherDataSuccess, responseError, true, true);
    }
}

function updateTeacherDataSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        myApp.alert(data.message,'Success', mainView.router.loadPage('teacher_profile_settings.html'));
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getCoursesDetail(){
    var requestDATA = {
        'request': 'all_courses'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getCoursesDetailSuccess, responseError, true, true);
}

function getCoursesDetailSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.courses.length; i++) {
            if (data.courses[i].course_image == '') {
                image = 'img/subjectPicture.png';
            } else {
                image = 'http://istudy.com.pk/assets/images/course/' + data.courses[i].course_image;
            }
            var html = '<li>';
            html += '<a href="#" class="item-link item-content" onClick="getCourseDescription('+data.courses[i].course_id+')">';
            html += '<div class="item-media"><img src="'+image+'" class="picture-icon" width="44" height="44"></div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+data.courses[i].course_title+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">Code: '+data.courses[i].course_code+'</div>';
            html += '<div class="item-subtitle">Teacher: '+data.courses[i].course_teacher+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
            $$("#all_subjects").append(html);
        }

        for (var i = 0; i < data.subjects.length; i++) {
            text = '<option value="' + data.subjects[i].subject_id + '">' + data.subjects[i].subject_name + '</option>';
            $$("#categoryID").append(text);
        }

    } else if (data.status == 'error'){
        var text = '<div class="content-block"><p>No Courses Found.</p></div>';
        $$("#all_subjects").append(text);
        requestFailure(data.status, data.message);
    } else if (data.status == '') {
        $$("#all_subjects").html('');
        requestFailure(data.status, data.message, showAlert);
    }
}

function getCourseDescription(course_id){
    console.log(course_id);
    var requestDATA = {
        'course_id': course_id,
        'request': 'course_detail'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getCourseDescriptionSuccess, responseError, true, true);
}

function getCourseDescriptionSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        text = '<div class="row">';
        for (var i = 0; i < data.courses.length; i++) {
            text += '<div class="col-100"><label class="checkbox-inline checkbox1"> <p class="checkboxLine"><input name="docs[]" class="checkbox" type="checkbox" value="' + data.courses[i].id + '||' + data.courses[i].price + '||' + data.courses[i].name + '||' + data.course_id + '">' + data.courses[i].name + ' (Rs, ' + data.courses[i].price + ')</p> </label></div>';
        }
        text += '</div>';
        myApp.alert(text, data.title, function () {
            addToCart();
        });
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function addToCart(id) {
    var items = 0;
    var already = '0';
    var this_loop = 0;
    var added_or_no = 0;

    var my_string = '';
    existing_cart = localStorage.getItem("cart");
    console.log(existing_cart);
    if (typeof localStorage.getItem("cart") !== 'undefined') {
        existing_cart = [];
    } else {
        existing_cart = JSON.parse(existing_cart);
    }

    console.log(existing_cart);

    $$('.checkbox:checked').each(function () {
        this_loop++;
        var will_add = '1';
        values = $$(this).val().split('||');
        my_string = {'course_id': values[3], 'doc_id': values[0], 'doc_title': values[2], 'doc_price': values[1]};
        console.log(my_string);
        if (existing_cart !== null && existing_cart.length > 0) {
            existing_cart.forEach(function (arrayItem) {
                if (arrayItem.course_id == values[3] && arrayItem.doc_id == values[0]) {
                    will_add = '0';
                    already = '1';
                }

            });
        } else {
            will_add = '0';
            items++;
            existing_cart.push(my_string);
        }

        if (will_add == '1') {
            added_or_no++;
            items++;
            existing_cart.push(my_string);
        }

    });


    ex_cart = JSON.stringify(existing_cart);

    localStorage.setItem('cart', ex_cart);

    var mycart = JSON.parse(ex_cart);

    console.log(mycart);

    console.log('items: ' + items);
    console.log('this loop : ' + this_loop);

    if (added_or_no != this_loop && added_or_no > 0) {
        text = 'Some items were already in cart.';
    } else if (items > 0) {
        text = 'Item added to cart.';
    } else if (already == '1') {
        text = 'Already in cart.';
    } else if (items == '0') {
        text = 'Nothing selected.';
    }

    //myApp.alert(text,'Info',null);

    myApp.modal({
        title:  'Info',
        text: text,
        buttons: [
        {
            text: 'View Cart',
            onClick: function() {
                mainView.router.loadPage('cart.html');
            }
        },
        {
            text: 'Okay',
            onClick: function() {
            }
        }
        ]
    })
    
}

function loadCart(){
    existing_cart = getItem("cart");
    if (existing_cart == '' || existing_cart == 'null') {
        existing_cart = [];
    } else {
        existing_cart = JSON.parse(existing_cart);
    }
    if (existing_cart !== null && existing_cart.length > 0) {
        $$('#cart_div').show();
        var total = '';
        existing_cart.forEach(function (arrayItem) {
            var text = '';
            text = '<li class="item-content"><div class="item-inner"><div class="item-title">' + arrayItem.doc_title + '</div><div class="item-after">' + arrayItem.course_id + '</div><div class="item-after">' + arrayItem.doc_price + '</div><div class="item-after"><i class="icon icon-cross" onclick="removeFromCart(' + arrayItem.course_id + ',' + arrayItem.doc_id + ')"></i></div></div></li>';
            total = Number(total) + Number(arrayItem.doc_price);
            $$('#cart_body').append(text);
        });
        text = '<tr><td></td><td class="text-center"><b>Total: </b></td><td class="text-center">' + total + '</td><td></td></tr>';
        $$('#cart_body').append(text);
    } else {
        $$('#cart_div').hide();
        text = '<tr><td colspan="4" class="text-center">Your cart is empty.</td></tr>';
        $$('#cart_body').append(text);
    }
}

function removeFromCart(course_id, doc_id) {
    var x = confirm("Are you sure you want to remove this item from your cart?");
    if (x) {
        $$('#cart_body').html('');
        existing_cart = localStorage.getItem("cart");

        if (existing_cart == '') {
            existing_cart = [];
        } else {
            existing_cart = JSON.parse(existing_cart);
        }

        existing_cart = existing_cart.filter(function (data, index) {
            return data.doc_id != doc_id
        });

        ex_cart = JSON.stringify(existing_cart);

        localStorage.setItem('cart', ex_cart);

        existing_cart = localStorage.getItem("cart");

        if (existing_cart == '') {
            existing_cart = [];
        } else {
            existing_cart = JSON.parse(existing_cart);
        }
        if (existing_cart !== null && existing_cart.length > 0) {
            var total = '';
            $$('#cart_div').show();
            existing_cart.forEach(function (arrayItem) {
                var text = '';
                text = '<li class="item-content"><div class="item-inner"><div class="item-title">' + arrayItem.doc_title + '</div><div class="item-after">' + arrayItem.course_id + '</div><div class="item-after">' + arrayItem.doc_price + '</div><div class="item-after"><i class="icon icon-cross" onclick="removeFromCart(' + arrayItem.course_id + ',' + arrayItem.doc_id + ')"></i></div></div></li>';
                total = Number(total) + Number(arrayItem.doc_price);
                $$('#cart_body').append(text);
            });
            text = '<tr><td></td><td class="text-center"><b>Total: </b></td><td class="text-center">' + total + '</td><td></td></tr>';
            $$('#cart_body').append(text);
        } else {
            $$('#cart_div').hide();
            text = '<p>Your cart is empty.</p>';
            $$('#cart_body').append(text);
        }
    } else {
        return false;
    }
}

function confirm_order() {
    existing_cart = localStorage.getItem("cart");
    if (existing_cart == '') {
        existing_cart = [];
    } else {
        existing_cart = JSON.parse(existing_cart);
    }
    ex_cart = JSON.stringify(existing_cart);
    placeOrder();
}

function placeOrder(){
    var requestDATA = {
        'app_user_id': localStorage.getItem('app_user_id'),
        'request': 'confirm_order',
        'items': ex_cart
    }
    myApp.showIndicator();
    serverRequest(requestDATA, placeOrderSuccess, responseError, true, true);
}

function placeOrderSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        localStorage.setItem("cart", "");
        myApp.alert(data.message,'Success',mainView.router.loadPage("cart.html"));
    }else if (data.status == 'error'){
        myApp.alert(data.message,'Error',null);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function searchCourses(){
    var requestDATA = {
        'search_type': $$('#search_type').val(),
        'CourseTitleOrId': $$('#search-navbar').val(),
        'categoryID': $$('#categoryID').val(),
        'request': 'search_courses'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, searchCoursesSuccess, responseError, true, true);
}

function searchCoursesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('#all_subjects').html('');
        console.log(data.courses.length);
        for (var i = 0; i < data.courses.length; i++) {
            if (data.courses[i].course_image == '') {
                image = 'img/subjectPicture.png';
            } else {
                image = 'http://istudy.com.pk/assets/images/course/' + data.courses[i].course_image;
            }
            var html = '<li>';
            html += '<a href="#" class="item-link item-content">';
            html += '<div class="item-media"><img src="'+image+'" class="picture-icon" width="44" height="44"></div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+data.courses[i].course_title+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">Code: '+data.courses[i].course_code+'</div>';
            html += '<div class="item-subtitle">Teacher: '+data.courses[i].course_teacher+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
            $$("#all_subjects").append(html);
        }
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function register(){

    if (($$('#password').val() == '') || ($$('#confirmPassword').val() == '') || ($$('#name').val() == '') || ($$('#email').val() == '') || ($$('#phone').val() == '') || ($$('#gender').val() == '') || ($$('#birthday').val() == '') || ($$('#address').val() == '') || ($$('#zip').val() == '')){
        myApp.alert('Please fill all fields.','Error',null);
    } else {
        var requestDATA = {
            'request': 'new_student_data',
            'name': $$('#name').val(),
            'email': $$('#email').val(),
            'phone': $$('#phone').val(),
            'password': $$('#password').val(),
            'confirmPassword': $$('#confirmPassword').val(),
            'gender': $$('#gender').val(),
            // 'school': $$('#school').val(),
            // 'class': $$('#class').val(),
            // 'country': $$('#country').val(),
            // 'state': $$('#state').val(),
            // 'city': $$('#city').val(),
            'birthday': $$('#birthday').val(),
            'address': $$('#address').val(),
            'zip': $$('#zip').val()
        }
    };
    myApp.showIndicator();
    serverRequest(requestDATA, registerSuccess, responseError, true, true);
}

function registerSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){

        myApp.alert(data.message, "Success", mainView.router.loadPage("login.html"));
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function sendEmail(){
    if ($$('#firstName').val() == '' || $$('#lastName').val() == '' || $$('#email').val() == '' || $$('#phone').val() == '' || $$('#my_message').val() == ''){
        myApp.alert('Please fill all fields.', "Alert", null);
    } else {
        var requestDATA = {
            'firstName': $$('#firstName').val(),
            'lastName': $$('#lastName').val(),
            'email': $$('#email').val(),
            'phone': $$('#phone').val(),
            'message': $$('#my_message').val(),
            'request': 'contact_us'
        }
        myApp.showIndicator();
        serverRequest(requestDATA, sendEmailSuccess, responseError, true, true);
    }
}

function sendEmailSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        myApp.alert(data.message,'Success',null)
    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function changePassword(){
    if ($$('#password').val() == '' || $$('#newPassword').val() == '' || $$('#confirmPassword').val() == ''){
        myApp.alert('Please fill all fields.', "Alert", null);
    } else if ($$('#newPassword').val() != $$('#confirmPassword').val()){
        myApp.alert('New Password and Confirm Password do not match.', "Alert", null);
    } else {
        var requestDATA = {
            'current_password': $$('#password').val(),
            'new_password': $$('#newPassword').val(),
            'confirm_password': $$('#confirmPassword').val(),
            'user_type': getItem('app_user_level'),
            'user_id' : getItem('app_user_id'),
            'request': 'change_password'
        }
        myApp.showIndicator();
        serverRequest(requestDATA, changePasswordSuccess, responseError, true, true);
    }
}

function changePasswordSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        myApp.alert(data.message,'Success',null)
    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getSchools(){
    var requestDATA = {
        'request': 'get_schools'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getSchoolsSuccess, responseError, true, true);
}

function getClasses(){
    var requestDATA = {
        'request': 'get_classes'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getClassesSuccess, responseError, true, true);
}

function getSubjects(){
    var requestDATA = {
        'request': 'get_subjects'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getSubjectsSuccess, responseError, true, true);
}


function getCountries(){
    var requestDATA = {
        'request': 'get_countries'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getCountriesSuccess, responseError, true, true);
}

function getStates(){
    if ($$('#country_id').val()!=""){
        $$('#state_id').html('<option value="">- Select State -</option>');
        $$('#city_id').html('<option value="">- Select City -</option>');
        $$('#state_id').val("");
        $$('#city_id').val("");
        var country_id = $$('#country_id').val();
    } else if (getItem('country')!="") {
        var country_id = getItem('country');
    }
    var requestDATA = {
        'request': 'get_states',
        'country_id' : country_id
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getStatesSuccess, responseError, true, false);
}

function getCities(){
    if ($$('#state_id').val()!=""){
        $$('#city_id').html('<option value="">- Select City -</option>');
        $$('#city_id').val("");
        var state_id = $$('#state_id').val();
    } else if (getItem('state')!="") {
        var state_id = getItem('state');
    }
    var requestDATA = {
        'request': 'get_cities',
        'state_id' : state_id
    }
    myApp.showIndicator();
    serverRequest(requestDATA, getCitiesSuccess, responseError, true, false);
}

function getSchoolsSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.schools.length; i++) {
            text = '<option value="' + data.schools[i].school_id + '">' + data.schools[i].school_name + '</option>';
            $$("#school_id").append(text);
        }
        $$('#school_id').val(getItem('school'));
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getClassesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.classes.length; i++) {
            text = '<option value="' + data.classes[i].class_id + '">' + data.classes[i].class_name + '</option>';
            $$("#class_id").append(text);
        }
        $$('#class_id').val(getItem('class'));
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getSubjectsSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.subjects.length; i++) {
            text = '<option value="' + data.subjects[i].subject_id + '">' + data.subjects[i].subject_name + '</option>';
            $$("#subject_id").append(text);
        }
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}


function getCountriesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.countries.length; i++) {
            text = '<option value="' + data.countries[i].country_id + '">' + data.countries[i].country_name + '</option>';
            $$("#country_id").append(text);
        }
        $$('#country_id').val(getItem('country'));
        if (getItem('country') !== null || getItem('state') !== null || getItem('city') !== null){
            getStates();
            getCities();
        }
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}


function getStatesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.states.length; i++) {
            text = '<option value="' + data.states[i].state_id + '">' + data.states[i].state_name + '</option>';
            $$("#state_id").append(text);
        }
        if ($$('#state_id').val()==''){
            $$('#state_id').val(getItem('state'));
        }
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getCitiesSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        for (var i = 0; i < data.cities.length; i++) {
            text = '<option value="' + data.cities[i].city_id + '">' + data.cities[i].city_name + '</option>';
            $$("#city_id").append(text);
        }
        if ($$('#city_id').val()==''){
            $$('#city_id').val(getItem('city'));
        }
    }else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    } else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

function getNewStates(){
    getStates();
}

function getNewCities(){
    getCities();
}

function loadDoc(type,url,name){
    setItem('doc_type',type);
    setItem('doc_url',url);
    setItem('doc_title',name);
    mainView.router.loadPage('course_content.html');
}

function getExperience(){
    var requestDATA = {
        'app_user_id': getItem('app_user_id'),
        'request': 'request_experience'
    }
    myApp.showIndicator();
    serverRequest(requestDATA, changePasswordSuccess, responseError, true, true);
}

function changePasswordSuccess(data, hideIndicator, showAlert){
    if(hideIndicator){
        myApp.hideIndicator();
    }
    console.log(data);

    if(data.status == 'success'){
        $$('.teacher_experience').html(data.experience);
    }   else if (data.status == 'error'){
        requestFailure(data.status, data.message, showAlert);
    }        else if (data.status == '') {
        requestFailure(data.status, data.message, showAlert);
    }
}

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}
function onBackKeyDown() {
    var sounds = document.getElementsByTagName('audio');
    for(i=0; i<sounds.length; i++) sounds[i].pause();
        if ((mainView.activePage.name=='student_profile') || (mainView.activePage.name=='teacher_profile') || (mainView.activePage.name=='login')){
            myApp.confirm('Are you sure you want to exit?', 'Alert', 
                function () {
                    onConfirm(1);
                },
                function () {
                    onConfirm(2);
                }
                );
        }   
        else {
            mainView.router.back();
        }
    }

    function onConfirm(button) {
    if(button==2){//If User selected No, then we just do nothing
        return;
    }else{
        navigator.app.exitApp();// Otherwise we quit the app.
    }
}

$(document).keypress(function(e) {
    if(e.which == 13) {
        if (mainView.activePage.name=='login'){
            login();
        }
        console.log('pressed');
        $('input').blur();
    }
});

function loadPDF(url){
    var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no');
    ref.addEventListener('loadstop', function() {
        ref.insertCSS({code: ".drive-viewer-toolstrip-rgt-panel{display: none !important;opacity: 0 !important;}"});
    });
}

function stopEverything(){
    var sounds = document.getElementsByTagName('audio');
    for(i=0; i<sounds.length; i++) sounds[i].pause();

        $("iframe").each(function() { 
            var src= $(this).attr('src');
            $(this).attr('src',src);  
        });
}